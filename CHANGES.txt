v1.0.8  Jan. 7, 2025:
    [Pr # 16] Python3.13 newly enables ssl.VERIFY_X509_STRICT by default now for SSL contexts. This adds a variety of requirements upon certificates, many of which test certificates currently using microserver do not comply with.
    [Pr # 15] Python3.13: Remove use of cgi
v1.0.7  Aug. 18, 2023:
    [Pr # 14] Python 3.12: ssl.wrap_socket -> context.wrap_socket
v1.0.6  June. 12, 2020:
    [Pr # 11] Exclude --lookupkey headers which don't appear in input session.log
    [Pr # 13] Allow response body for POST requests
v1.0.5  June. 12, 2020:
    [Pr # 12] Adding SO_REUSEADDR to socket binds.
v1.0.4  Oct. 23, 2019:
    [Pr #10] Allowing keep-alive instead of always closing the connection
    Fix to setup.py logic to not crash on because of missing dependancy
v1.0.3  Sep. 26, 2019:
    [Pr #8] Fixed issue with default sigINT handler not existing odd CI cases.
v1.0.2  Aug. 6, 2019:
    Fixed a bug where Microserver isn't terminating on sigINT by force closing every connection after response
v1.0.1  Jan. 4, 2019:
    Fixed bugs where SSL server was starting on the nonSSL server's port and fixed up the read hook logic
v1.0.0  Dec. 3, 2018:
    Initial release